/**
 * Type casting is process to convert a variable from one data type to another data type.
 * For example if we want to store an integer value in a float variable
 * then we need to typecast integer into float.
 */

#include <stdio.h>

void main() {
    int x = 10;
    char y = 'a';

    // y implicitly converted to int. ASCII value of 'a' is 97
    x = x + y;

    // x is implicitly converted to float
    float z = x + 1.0;

    printf("x = %d, z = %f\n", x, z);


    double w = 1.2;
    // Explicitly conversion from double to int
    int total = (int)w + 1;
    printf("Sum = %d\n", total);
}
