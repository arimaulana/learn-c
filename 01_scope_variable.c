#include <stdio.h>

int a; // global variable

void main() {
    int b; // local variable
    a = 10;
    b = 20;
    printf("Value of a: %d\n", a);
    printf("Value of b: %d\n", b);
}
