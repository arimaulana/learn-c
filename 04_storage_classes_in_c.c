/**
 * Storage class
 *
 * will provides following information to compiler
 *
 * 1. Storage area of variable
 * 2. Scope of variable that is in which block the variable is visible
 * 3. Life time of a variable that is how long the variable will be there in active mode.
 * 4. Default value of a variable if it is not initialized it.
 *
 * Type of storage class
 * Type         | Storage place | Scope     | Life                              | Default Value
 * - auto       | CPU Memory    | body      | Within the Function               | Garbage value
 * - extern     | CPU Memory    | function  | program                           | 0 (zero)
 * - static     | CPU Memory    | program   | Till the end of the main program  | 0 (zero)
 * - register   | Register Memo | body      | Within the Function               | Garbage value
 */

#include <stdio.h>


void increment_auto();
void increment_static();

int x = 20;

void main() {
    printf("auto Storage Class\n");
    printf("auto storage class is the default storage class for all local variables.\n");
    printf("the scope auto variable is within the function.\n");
    printf("It is equivalent to local variable.\n");
    increment_auto();
    increment_auto();
    increment_auto();

    printf("\n\nstatic Storage Class\n");
    printf("The static storage class instructs the compiler to keep a local variable\n");
    printf("in existence during the life-time of the program instead of creating\n");
    printf("and destroying it each time it comes into and goes out of scope.\n");
    increment_static();
    increment_static();
    increment_static();

    printf("\n\nextern Storage Class\n");
    printf("The extern storage class is used to give a reference of a global variable\n");
    printf("that is visible to ALL the program files.\nIt is equivalent to global variable.\n");
    extern int y;
    printf("The value of x is %d \n", x);
    printf("The value of y is %d \n", y);

    printf("\n\nRegister variable\n");
    register int a = 10;
    ++a;
    printf("\n value of a: %d", a);
    printf("Enter a value: ");
    scanf("%d", &a);
    --a;
    printf("\n value of a: %d", a);
}

int y = 30;

void increment_auto() {
    auto int i = 0;
    printf(" %d ", i);
    i++;
}

void increment_static() {
    static int i = 0;
    printf(" %d ", i);
    i++;
}
