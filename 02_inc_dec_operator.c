#include <stdio.h>

void main() {
    int x, i;
    i = 10;
    x = ++i;
    printf("Pre-Increment\n");
    printf("first, increment the value of variable and \nthen used inside the expression (initialize into another variable).\n");
    printf("x: %d\n", x);
    printf("i: %d\n", i);

    printf("Post-Increment\n");
    printf("first, value of variable is used in the expression (initialize into another variable) and \nthen increment the value of variable.\n");
    int y, j;
    j = 10;
    y = j++;
    printf("y: %d\n", y);
    printf("i: %d\n", j);
}
