/**
 * strcpy can be used to copy one string to another.
 * #define DEST_SIZE 40
 * char src[] = "Look Here";
 * char dest[DEST_SIZE] = "Unimaginable";
 * strcpy(dest, src);
 * printf(dest); Output: Look Here
 * 
 * Warning: The destination character array must be large enough to hold all characters in source character array + NULL character.
 * if the source array has 100 characters, the dest array must be at least 101 characters long.
 */

#include <stdio.h>
#include <string.h>

#define DEST_SIZE 40

int main()
{
    printf("strcpy test\n");
    char src[] = "Look Here";
    const char dest_backup[DEST_SIZE] = "Unimaginable";
    char dest[DEST_SIZE + 1] = "Unimaginable"; // should be plus one to make sure it larger than source

    char *p = dest + 5; // pointing to dest at index 5 (6th character of dest)

    strcpy(p, src); // cpy src into p (start at index 5)
    printf("%s\n", dest);

    // reset data
    strcpy(dest, dest_backup);
    // it can also pointing into any index in the source character
    char *ps = src + 4; // pointing to src at index 4 (5th character of src)
    char *pd = dest + strlen(dest); // pointing to the null character of dest
    strcpy(pd, ps);
    printf("%s\n", dest);
    // // Warning: Care must be taken when passing character pointers to strcpy. The source and destination aren't allowed to overlap.
    // char *sp = dest + 5;
    // char *dp = dest + 8;
    // strcpy(dp, sp);
    // printf("%s\n", dest);
    // // but when practicing it out, the output become Unimaginginable which is normal? maybe its become normal depends on what my compiler use and it different with one used in tech.io

    printf("\nstrncpy test\n");
    // strncpy is used to copy first several characters from source string to destination string.
    // it doesn't append any NULL character when the copying finishes
    // it would stop the copying character when it encounter the NULL character in the source
    char srcn[] = "Look Here"; // src has 9 + 1 (null) = 10 characters
    char destn[9]; // dest can only hold 9 characters

    strncpy(destn, srcn, 9); // first 9 characters are copied to dest, where is the NULL character?
    // printf("%s\n", destn); // in tech.io, it produce Look HereLook Here. it because destn doesnt have NULL character

    // to really printing out destn, one solution is to accessing each character individually and printing them out
    for (int i = 0; i < 9; i++)
    {
        printf("%c", destn[i]);
    }
    printf("\n");

    return 0;
}