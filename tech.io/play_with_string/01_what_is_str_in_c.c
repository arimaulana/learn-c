/**
 * A string in C (also known as C string) is an array of characters, followed by a NULL character.
 * To represent a string, a set of characters are enclosed within double quotes (").
 * The C string "Look Here" would look like as follows in memory:
 * ------------------------------------------
 * | L | o | o | k |   | H | e | r | e | \0 |
 * ------------------------------------------
 * 
 * It's an array of 10 characters.
 * 
 * There are different ways to init a variable to access C string
 * char *char_ptr = "Look Here"; or
 * char char_ptr[] = "Look Here"; or
 * char char_ptr[] = { 'L', 'o', 'o', 'k', ' ', 'H', 'e', 'r', 'e', '\0' };
 * 
 * or you can input size for character inside []. but make sure to have it large enough (including null character)
 * #define ARRAY_SIZE 15
 * char char_ptr[ARRAY_SIZE] = "Look Here"; the index 9 until 14 would be NULL characters
 */