#include <stdio.h>
#include <string.h>

int main()
{
    char strnull[] = { 'L', 'o', 'o', 'k', '\0', 'H', 'e', 'r', 'e', '\0' }; // would be cut until 'Look' only (cause it has NULL character after)
    char str1[] = "Look Hera";
    char str2[] = "Look Here";
    char str3[] = "Look Here";
    char str4[] = "Look Her";
    char str5[] = "Look";

    // strlen calculate the size character exlucde NULL character
    printf("Number of characters in strnull (\'%s\') is %ld\n", strnull, strlen(strnull));
    printf("Number of characters in \'%s\' is %ld\n", str2, strlen(str2));

    /**
     * how strcmp works is
     * loop every characters to compare each character
     *  compare is done by substract char from first string with second string (ASCII value 1st - ASCII value 2nd)
     *  so the result would be negative, 0 (equal), or positive based on ASCII value comparison.
     *  while comparing each char one by one, if the result non zero, the compare stop looping and just return the result.
     */

    printf("%d\n", strcmp(str1, str2)); // (ASCII of a - ASCII of e)
	printf("%d\n", strcmp(str2, str3)); // dont have any different ascii character value
	printf("%d\n", strcmp(str3, str4)); // (ASCII of e - ASCII of null)
    printf("%d\n", strcmp(str4, str5)); // (ASCII of space - ASCII of null)
    printf("%d\n", strcmp(strnull, str3)); // In this case, the string become 'Look' only (have null character), so it compare (ASCII of null - ASCII of space)

    // If you want to compare first-n characters of two strigns, then strncmp can be used. Its return value is similar to strcmp
    printf("Compare first N characters of two string using strncmp\n");
    printf("%d\n", strncmp(str1, str2, 8));
    printf("%d\n", strncmp(str1, str2, 9));

    return 0;
}