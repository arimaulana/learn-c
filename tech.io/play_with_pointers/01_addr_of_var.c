/**
 * A program being executed by a processor has two masjor parts - the code and the data.
 * The code section is the code you've written and the data section holds the variables you're using in  the program.
 * 
 * All code and variables are loaded into memory (usually RAM) and the processor executes the code from there.
 * Each segment (usually a byte) in the memory has an address - whether it holds code or variable - that's the way for the processor to access the code and variables.
 * 
 * For example, consider a simple program of adding two numbers.
 * When the program will run, the processor will save these two numbers in two different memory locations.
 * Adding these numbers can be achieved by adding the contents of two different memory locations.
 * 
 * A memory location where data is stored is the address of that data. In C address of a variable can be obtained by prepending the character & to a variable name.
 */

#include <stdio.h>

void f(int p)
{
    printf("The address of p inside f() is %p\n", &p);
}

void g(int r)
{
    printf("The address of r inside g() is %p\n", &r);
    f(r);
}

int main()
{
    int a = 55;

    printf("The address of a inside main() is %p\n", &a);
    f(a);
    g(a);

    return 0;
}