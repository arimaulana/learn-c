#include <stdio.h>
#include <string.h>

int main()
{
    char str[] = "Yet another example";
    char *p = str + strlen(str); /* p points to the NULL character */
    p--; /* Now p points to the last character */

    int len = strlen(str);
    for (int i = 0; i < len; i++, p--)
    {
        printf("%c", *p);
    }

    return 0;
}
