/**
 * An array is a contiguous memory location which holds data.
 * The data can be integers, characters, floating point numbers, structures etc.
 * Each array element takes a distinct memory location.
 * The all have distinct address.
 * 
 * In the following example, int32_t data type is used.
 * int32_t represents 32 bit integer and is defined in stdint.h header file in supported compilers.
 * While int data type doesn't guarantee about its size, int32_t is guaranteed to be 4 bytes long in every compiler that supports it.
 * If your compiler doesn't support int32_t data type, try an integer type(int, long, long long) which is 32 bit wide in your environment.
 * Although according to C standard compilers aren't forced to implement int32_t data type, to make the size of integers predictabl,
 * int32_t type is used in following examples in this tutorial.
 */

#include <stdio.h>
#include <stdint.h>

#define ARRAY_SIZE 3

int main()
{
    int32_t arr[ARRAY_SIZE] = {22, 33, 44};

    printf("Address of arr %p\n", arr);

    printf("Addresses of array elements:\n");
    for (int i = 0; i < ARRAY_SIZE; i++)
    {
        /**
         * Do you know that replacing &arr[i] with &i[arr] also works
         * it works because for compiler arr[i] and i[arr] are same operation.
         * Compiler converts arr[i] to arr + i and similarly i[arr] to i + arr.
         * 
         * What does arr + i mean? It is the address of the i-th element of the memory location pointed to by arr.
         * This sounds like arr is a pointer.
         * In fact, an array name in C acts as a pointer to the first element of the array!
         * 
         * for more information
         * https://stackoverflow.com/questions/381542/with-arrays-why-is-it-the-case-that-a5-5a
         */
        printf("Index %d: %p\n", i, &arr[i]);
        // printf("Index %d: %p\n", i, &i[arr]); // its the same as above. just for fun
        printf("Index %d: %p | %p\n", i, &arr[i], arr + i);
    }

    return 0;
}