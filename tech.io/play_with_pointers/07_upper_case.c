#include <stdio.h>
#include <ctype.h> // ctype header file useful for testing and mapping characters

void uppercasing_str(char *pstr)
{
    while (pstr != NULL && *pstr != '\0') // pstr itself cannot be NULL, a NULL pointer cannot be dereferenced. Continue only if this is not the end of the string
    {
        *pstr = toupper(*pstr); // convert original array content
        pstr++; // points to next character
    }
}

int main()
{
    char str[] = "a small string";
    uppercasing_str(str);
    printf("%s\n", str);

    return 0;
}