#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

// Constant definitions
#define MAX_NAME_SIZE 1000
#define MAX_STD_SIZE 30

// student records
typedef struct
{
    uint32_t id;
    char name[MAX_NAME_SIZE];
    uint32_t passing_year;
} Student;

/**
 * sort_by_passing_year
 * 
 * add method to sort by passing year
 * this function would sort the data and modify its original data into sorted
 */
int sort_by_passing_year(const void* a, const void* b)
{
    const Student *sa = a;
    const Student *sb = b;

    // `->` operator means 'dereference and access member'
    // a->b is equivalent to (*a).b
    // for more info https://www.quora.com/What-does-mean-in-C-C++-programming
	return sa->passing_year - sb->passing_year;
}

/**
 * sort_by_name
 * 
 * how it works,
 * name with less characters can be considered to come first.
 * if the lengths of the names are same, then a character by character comparison needs to be performed to determine which name comes first.
 */
int sort_by_name(const void* a, const void* b)
{
    const Student *sa = a;
    const Student *sb = b;

    int lena = strlen(sa->name);
    int lenb = strlen(sb->name);

    if (lena == lenb)
    {
        for (int i = 0; i < lena; i++)
        {
            if (sa->name[i] != sb->name[i])
            {
                return sa->name[i] - sb->name[i];
            }
        }

        // name are same
        return 0;
    }

    return lena - lenb;
}

void getAll(Student* std_list, size_t len)
{
    for (int i = 0; i < len; i++)
    {
        Student data_std = std_list[i];
        printf("%5d %5d %s\n", data_std.id, data_std.passing_year, data_std.name);
    }
}

int main()
{
    // Init seed for pseudo random number generator
    srand(time(0));

    // Database of students
    Student std_db[MAX_STD_SIZE];

    // Init everything to 0
    memset(std_db, 0, sizeof(std_db));

    // Randomly populate students
    for (uint32_t i = 0; i < MAX_STD_SIZE; i++)
    {
        // Assign unique id
        std_db[i].id = i + 1;

        // Randomly populate name
        int max_name_size = (MAX_NAME_SIZE < 10 ? MAX_NAME_SIZE : 10);
        int name_size = rand() % max_name_size + 5;
        for (int j = 0; j < name_size; j++)
        {
            std_db[i].name[j] = rand() % 26 + 'a';
        }

        // Randomly populate passing year from 1999 to 2018
        std_db[i].passing_year = 2018 - rand() % 20;
    }

    // Print unsorted data students
    printf("Students unsorted data:\n\n");
    getAll(std_db, MAX_STD_SIZE);

    // sort by passing year
    qsort(std_db, MAX_STD_SIZE, sizeof(std_db[0]), sort_by_passing_year);
    printf("\nStudents sorted by passing year:\n\n");
    getAll(std_db, MAX_STD_SIZE);

    // sort by name
    qsort(std_db, MAX_STD_SIZE, sizeof(std_db[0]), sort_by_name);
    printf("\nStudents sorted by name:\n\n");
    getAll(std_db, MAX_STD_SIZE);

    return 0;
}