/**
 * C has a type of variable which can store address or memory location.
 * This type of variable is called a pointer.
 * Essentially a pointer holds address of a variable.
 * To declare a variable as a pointer, * is used before the variable name.
 * 
 * A pointer is also called reference type variable in generic way.
 * int *ptr_of_a = &a; can be stated like ptr_of_a is a pointer to an integer of a.
 * ptr_of_a points to a. If a has content of integers, ptr_of_a has a content (that's the address of a).
 * As ptr_of_a itself is a variable, it has an address too.
 * 
 * The content of a can be obtained using ptr_of_a. This is called dereferencing a pointer.
 * Dereferencing a pointer can be performed by a * character before the pointer variable name.
 */

#include <stdio.h>

int main()
{
	int a = 55;			/* An integer */
	int *pa = &a;		/* A pointer to an integer */
	int **ppa = &pa;	/* A pointer to a pointer to an integer */

    printf("The content of a              is %d\n", a);
	printf("The address of a              is %p\n", &a);

	printf("The content of pa             is %p\n", pa);
	printf("The address of pa             is %p\n", &pa);
	printf("The dereferenced value of pa  is %d\n", *pa);

	printf("The content of ppa            is %p\n", ppa);
	printf("The address of ppa            is %p\n", &ppa);
	printf("The dereferenced value of ppa is %p\n", *ppa);
	printf("The content of a through ppa  is %d\n", **ppa);

	return 0;
}

/**
 * Referencing/pointing
 * address of a  == content of pa
 * address of pa == content of ppa
 * 
 * Dereferencing
 * dereferenced value of ppa    == content of pa
 * dereferenced value of pa     == content of a
 * double dereferencing of ppa  == content of a
 */
