#include <stdio.h>
#include <stddef.h>

#define ARRAY_SIZE 10

int main()
{
	int arr[ARRAY_SIZE] = { 0 };

	int *p = arr + 2;
	int *q = arr + 6;

	ptrdiff_t diff = q - p;
	printf("Difference %ld\n", diff);

	return 0;
}