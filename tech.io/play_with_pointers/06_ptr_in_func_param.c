/**
 * The `sizeof` operator takes a parameter and returns the size of the parameter in bytes.
 * Following example prints the size of an array with 3 elements, each is a 32 bit integer.
 */

#include <stdio.h>
#include <stdint.h>

#define ARRAY_SIZE 3

void print_array_size(int32_t* parr)
{
    size_t size = sizeof(parr);
    printf("Size of parameter is %ld bytes\n", size);
}

int main()
{
    int32_t arr[ARRAY_SIZE] = { 22, 33, 44 };

    size_t size = sizeof(arr);
    printf("Actual array size %ld bytes\n", size);

    print_array_size(arr);

    return 0;
}