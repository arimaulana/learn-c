/**
 * When a variable is declared, compiler automatically allocates memory for it.
 * This is known as compile time memory allocation or static memory allocation.
 * Memory can be allocated for data variables after the program begins execution.
 * This mechanism is known as runtime memory allocation or dynamic memory allocation.
 * 
 * The memory allocated at compile time is deallocated (i.e. the memory is `released` so that the same memory
 * can be reused by other data) automatically at the end of the scope of the variable. In the example below the memory allocated for the variable
 * i will be deallocated when the closing brace (}) of the statement block is encountered (considering no compiler optimization)
 * 
 * {
 *  int i = 0;
 * }
 * 
 * C provides several functions in stdlib library for dynamic memory allocation.
 * It's easy to both use and misuse these functions.
 * ATTENTIONS!!! One should write a program following best coding practices when dynamic memory allocation library functions are used.
 * 
 * Memory Allocation With `calloc` (number_of_elem, size_each_elem)
 * when using calloc, if fails allocating memory, it would return NULL, else (success) it will initializes all bits to 0.
 * 
 * malloc (number of bytes) => pointer to the first address of the allocated region. It wouldn't initialize the allocated memory.
 * 
 * realloc (ptr_arr_that_would_be_realloc, new_size_in_bytes)
 * 
 * free used for deallocating memory
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>

#define NUMBER_OF_ELEMENTS 100

int main()
{
    /** Initialize seed for pseudo random number generator */
    srand(time(0));

    int32_t *parr = calloc(NUMBER_OF_ELEMENTS, sizeof(int32_t));

    if (parr == NULL) // Memory allocation failes
    {
        printf("Couldn't allocate memory\n");
    }
    else // Memory allocation successful
    {
        printf("Memory allocation successful\n");
        printf("Size parr %d\n", sizeof(parr));

        // Generate and store random numbers
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            parr[i] = rand();
        }

        printf("Before allocation: %d %d %d\n", parr[3], parr[7], parr[11]);

        // Reduce the array size to half
        parr = realloc(parr, (NUMBER_OF_ELEMENTS / 2) * sizeof(int32_t));

        if (parr == NULL)
        {
            printf("Memory reallocation fails\n");
        }
        else
        {
            printf("Memory reallocation successful\n");

            // Proof that realloc keeps old array contents
            printf("After reallocation: %d %d %d\n", parr[3], parr[7], parr[11]);
        }
    }

    printf("sizeof parr = %d\n", sizeof(parr));

    free(parr);
    parr = NULL;

    return 0;
}

