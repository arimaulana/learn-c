#include <stdio.h>

long int factorial(int n);

void main() {
    int i, f, num;

    printf("Enter any number: ");
    scanf("%d", &num);
    f = factorial(num);
    printf("\nFactorial of %d is %d\n", num, f);
}

long int factorial(int n) {
    return n >= 1 ? n * factorial(n - 1) : 1;
}
