#include<stdio.h>
#include<unistd.h>

void function1();
void function2();

#pragma startup function1
#pragma exit function2

void main() {
    sleep(2);
    printf("\nI am main function");
}

void function1() {
    sleep(1);
    printf("\nI am function1");
}

void function2() {
    sleep(2);
    printf("\nI am function2");
}
