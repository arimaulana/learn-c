/**
 * Structure is a user defined data type which hold or store
 * heterogeneous data item or element in a single variable.
 * It is a Combination of primitive and derived data type.
 */

#include <stdio.h>

struct emp {
    int id;
    char name[36];
    float sal;
};

void main() {
    struct emp e;

    printf("Enter employee ID, Name, Salary: ");
    scanf("%d", &e.id);
    scanf("%s", &e.name);
    scanf("%f", &e.sal);

    printf("ID: %d", e.id);
    printf("\nName: %s", e.name);
    printf("\nSalary: %f", e.sal);
}
