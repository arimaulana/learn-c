/**
 * GIF Animation to differenciate concept call by value and reference
 * https://www.sitesbay.com/cprogramming/images/function/call-by-value-call-by-reference.gif
 */

#include <stdio.h>

void swap_value(int a, int b) {
    int temp;
    temp = a;
    a = b;
    b = temp;
}

void swap_ref(int *a, int *b) {
    int temp;
    temp = *a;
    *a = *b;
    *b = temp;
}

void main() {
    int a = 100;
    int b = 200;

    swap_value(a, b);
    printf("Value of a: %d", a);
    printf("\nValue of b: %d", b);

    swap_ref(&a, &b);
    printf("\nValue of a: %d", a);
    printf("\nValue of b: %d\n", b);
}
