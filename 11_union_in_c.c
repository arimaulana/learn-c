/**
 * Union, almost same with structure
 *
 * Advantage of union over structure
 *  It occupies less memory because it occupies the memory of largest member only.
 *
 * Disadvantage of union over structure
 *  It can store data in one member only.
 *
 * Memory Allocation in Structure and Union
 * Structure allocates separate storage space for its every members.
 * Union allocates one common storage space for its all members.
 * Union find which member need more memory than other member,
 * then it allocate that much space
 *
 *
 * When use Structure and Union
 *
 * When need to manipulate the data for all member variables then use structure.
 * When need to manipulate only one member then use union.
 *
 */

#include <stdio.h>

union emp {
    int ID;
    char name[15];
    double salary;
}u;

void main() {
    printf("Enter emp ID: ");
    scanf("%d", &u.ID);
    printf("Enter emp Name: ");
    scanf("%s", &u.name);
    printf("%f", &u.salary);
    printf("Emp ID: %d", u.ID);
    printf("Emp Name: %s", u.name);
    printf("Emp Salary: %f", u.salary);
}
