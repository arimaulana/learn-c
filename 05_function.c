#include<stdio.h>

int sum(int a, int b); // declaring function

void main() {
    int a = 10;
    int b = 20;
    int c;

    c = sum(a, b);
    printf("%d + %d = %d\n", a, b, c);
}

int sum(int a, int b) { // defining function
    return a + b;
}
