/**
 * Temporary storage area is called buffer.
 *
 * All standard input output devices are containing input output buffer.
 *
 * flushall()
 * a predefined function which is declared in stdio.h. by using flush all
 * we can remove the data from standard i/o buffer.
 *
 * fflush()
 * predefined function in "stdio.h" header file used to flush or clear either i/o buffer memory.
 *
 * fflush(stdin)
 * used to clear the input buffer memory. It is recommended to use before writing scanf statement.
 *
 * fflush(stdout)
 * used to clear the output buffer memory. It is recommended to use before printf statement.
 */

#include <stdio.h>

void main() {
    printf("Without clearing buffer\n");
    int val1, val2;
    printf("Enter v1: ");
    scanf("%d", &val1);
    printf("\nEnter v2: ");
    scanf("%d", &val2);
    printf("\nv1 + v2 = %d\n", val1 + val2);

    fflush(stdout);
    printf("Clearing buffer\n");
    int v1, v2;
    printf("\nEnter v1: ");
    fflush(stdin);
    scanf("%d", &v1);
    printf("\nEnter v2: ");
    fflush(stdin);
    scanf("%d", &v2);
    printf("\n v1 + v2 = %d\n", v1 + v2);
}


