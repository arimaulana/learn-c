#include <stdio.h>

void main() {
    char name[15];
    printf("Enter name: ");
    gets(name); // gets are used to get input as a string, including whitespace (scanf would be terminated if have whitespace char)
    printf("Your name is: ");
    puts(name);
}
