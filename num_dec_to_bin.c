/**
 * C Program to Convert a Number Decimal System into Binary System using Recursion
 */

#include <stdio.h>
#include <string.h>

#define MAX_STRING_SIZE 40

int convert(int);

int main() {
    int dec, bin;
    char str_bin[MAX_STRING_SIZE];

    printf("Enter a decimal number: ");
    scanf("%d", &dec);
    bin = convert(dec);
    printf("The binary equivalent of %d is %d.\n", dec, bin);
    
    // convert bin into string
    sprintf(str_bin, "%d", bin);

    // split number
    char delim[] = "1";
    char *ptr = strtok(str_bin, delim);

    while (ptr != NULL) {
        printf("'%s'\n", ptr);
        ptr = strtok(NULL, delim);
    }

    return 0;
}

int convert(int dec) {
    return dec == 0 ? 0 : (dec % 2 + 10 * convert(dec / 2));
}