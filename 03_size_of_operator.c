#include <stdio.h>

void main() {
    int a;
    float b;
    double c;
    char d;

    printf("Size of Integer: %d bytes\n", sizeof(a));
    printf("Size of Float: %d bytes\n", sizeof(b));
    printf("Size of Double: %d bytes\n", sizeof(c));
    printf("Size of character: %d bytes\n", sizeof(d));
}
