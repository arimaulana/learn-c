/**
 * In CLI arguments application main() function will takes two arguments that is argc and argv
 *
 * argc (ARGument Counter) is an integer type variable and it holds total number of arguments which is passed into main function.
 * It take Number of arguments in the cli including program name.
 * argv[] (ARGument Vector) is a char* type variable, which holds actual arguments which is passed to main function.
 */

#include <stdio.h>

void abv(int argc, char **argv);

void main(int argc, char* argv[]) {
    int i;
    printf("Total number of arguments: %d", argc);

    for (i = 0; i < argc; i++) {
        printf("\n %d argument: %s", i, argv[i]);
    }

    abv(argc, argv);
}

void abv(int argc, char **argv) {
    int i;
    printf("data in abv: ");
    printf("\n Total no. of arguments: %d", argc);
    for (i = 0; i < argc; i++) {
        printf("\n %d argument: %s", i+1, argv[i]);
    }
}
