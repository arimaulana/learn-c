#include <stdio.h>

void main() {
    int i, marks[] = {80, 62, 70, 90, 98};

    printf("size of marks: %d", sizeof(marks));

    for (i = 0; i < 5; i++) {
        printf("\n%d", marks[i]);
    }
}
