#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/**
 *  * Auto-generated code below aims at helping you parse
 *   * the standard input according to the problem statement.
 *    **/
int main()
{
    char source[101];
    fgets(source, 101, stdin);
    char target[101];
    fgets(target, 101, stdin);

    int len = sizeof(source) / sizeof(source[0]);
    printf("%s", source);
    for (int i = 0; i < len; i++) {
            int letter = source[i];
            int t_letter = target[i];

            // perlu set break apabila sudah bertemu null termination. karena akan terus compare jika tidak pakai.
            // ini diakibatkan ketika declare variable, kita declare dengan kapasitas yang besar. [101]
            if (letter == '\0') {
                break;
            }

            if (letter != t_letter) {
                        source[i] = t_letter;
                        printf("%s", source);
                    }
                }

        return 0;
}
