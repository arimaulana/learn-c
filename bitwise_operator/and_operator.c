/**
 * The output of bitwise AND is 1 if the corresponding bits of two operands is 1.
 * If either bit of an operand is 0, the result of corresponding bit is evaluated to 0.
 *
 * Let us suppose the bitwise AND operation of two integers 12 and 25.
 *
 * 12 = 00001100 (In Binary)
 * 25 = 00011001 (In Binary)
 *
 * Bit operation of 12 and 25
 *    00001100
 * &  00011001
 *    --------
 *    00001000 = 8 (In decimal)
 */
#include <stdio.h>
int main () {
    int a = 12, b = 25;
    printf("Output = %d", a & b);
    return 0;
}
